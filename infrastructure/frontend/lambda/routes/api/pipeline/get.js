const AWS = require('aws-sdk');

const { tables } = require('../../../utils/config').aws.dynamoDB;

const docClient = new AWS.DynamoDB.DocumentClient();

module.exports = async(req, res, next) => {
  try {
    const { name } = req.query;
    const { Item = {} } = await docClient.get({
      TableName: tables.users,
      Key: {
        user: req.session.user,
      },
    }).promise();
    const { configs = [] } = Item;
    const config = configs.find((conf) => conf.name === name);
    if (typeof config === 'undefined') {
      throw new TypeError(`Unable to find config with name: ${name}`);
    }
    res.json({ data: config });
  } catch (e) {
    console.error('err', e.trace, e.code, e.stack);
    next();
  }
};
