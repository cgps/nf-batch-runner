// all utils without deps

exports.clearSession = (req, res) => {
  res.clearCookie('sid');
  res.clearCookie('user');
  req.session.destroy();
};
