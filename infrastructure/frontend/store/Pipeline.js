import { setPipeline, getPipelines, runPipeline } from './helpers/api';

export const state = () => ({
  pipelines: [],
  inputs: [],
  loading: true,
  name: undefined,
  config: undefined,
  selection: undefined,
});

export const getters = {
  cast: () => (type, value) => {
    switch (type) {
      case 'integer':
        return Number.parseInt(value);
      case 'number':
        return Number.parseFloat(value);
      case 'boolean':
        return Boolean(value);
      case 'list':
      case 'map':
      case 'text':
      case 'file':
      case 'dir':
      default:
        return String(value);
    }
  },
  pipeline(state, getters) {
    const pipelineIndex = state.pipelines.findIndex(({ name }) => name === state.selection);
    if (pipelineIndex < 0) {
      return [];
    }
    const { name, job, command, inputs, parameters } = state.pipelines[pipelineIndex];
    return {
      name,
      job,
      command,
      inputs: state.inputs
        .filter((_, index) => inputs.includes(index))
        .map((input) => {
          const param = parameters.find((parameter) => parameter.name === input.name);
          if (typeof input.value !== 'undefined') {
            param.value = getters.cast(param.type, input.value);
          } else if (typeof param.default !== 'undefined') {
            param.value = getters.cast(param.type, param.default);
          }
          return param;
        }),
    };
  },
  jobs: ({ pipelines = [] }) => pipelines.map(({ job }) => job),
};

export const mutations = {
  setPipelines(state, value) {
    state.pipelines = value;
  },
  setInputs(state, value) {
    state.inputs = value;
  },
  setName(state, value) {
    state.name = value;
  },
  setLoading(state, value) {
    state.loading = value;
  },
  setConfig(state, value) {
    state.config = value;
  },
  setSelection(state, value) {
    state.selection = value;
  },
  setJob(state, value) {
    const pipelineIndex = state.pipelines.findIndex((item) => item.name === state.selection);
    if (pipelineIndex < 0) {
      console.error('Failed to find pipeline');
      return;
    }
    state.pipelines[pipelineIndex].job = value;
  },
  setInput(state, { name, value }) {
    const pipelineIndex = state.pipelines.findIndex((item) => item.name === state.selection);
    if (pipelineIndex < 0) {
      console.error('Failed to find pipeline');
      return;
    }
    const { inputs = [] } = state.pipelines[pipelineIndex];
    const inputIndex = state.inputs.findIndex((input, index) => input.name === name && inputs.includes(index));
    if (inputIndex < 0) {
      console.error('Failed to find input');
      return;
    }
    state.inputs[inputIndex].value = value;
  },
  appendPipeline(state, value) {
    state.pipelines.push(value);
  },
};

export const actions = {
  // list all available pipelines
  async getPipelines({ commit }) {
    commit('setLoading', true);
    const { data = [] } = await getPipelines();
    const inputs = [];
    const pipelines = data.map((pipeline) => {
      const { parameters = [] } = pipeline;
      pipeline.inputs = [];
      parameters.forEach((param) => {
        const len = inputs.push({ name: param.name, value: undefined });
        pipeline.inputs.push(len - 1);
      });
      return {
        ...pipeline,
        job: '',
      };
    });
    commit('setInputs', inputs);
    commit('setPipelines', pipelines);
    commit('setLoading', false);
  },
  async setPipeline({ commit, state }, { name, config }) {
    commit('setLoading', true);
    const { data = {} } = await setPipeline({ name, config });
    const inputs = [ ...state.inputs ];
    const { parameters = [] } = data;
    data.inputs = [];
    parameters.forEach((param) => {
      const len = inputs.push({ name: param.name, value: undefined });
      data.inputs.push(len - 1);
    });
    data.name = name;
    commit('setInputs', inputs);
    commit('appendPipeline', data);
    commit('setName', undefined);
    commit('setConfig', undefined);
    commit('setSelection', name);
    commit('setLoading', false);
  },
  async runPipeline({ getters, commit }) {
    commit('setLoading', true);
    const { pipeline = {} } = getters;
    const { name, job, command, inputs = [] } = pipeline;
    const payload = {
      name,
      job,
      command,
      inputs,
    };
    await runPipeline(payload);
    commit('setLoading', false);
  },
};
